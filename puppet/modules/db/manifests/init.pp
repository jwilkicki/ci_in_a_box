# Class: db
# ===========================
#
# Full description of class db here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'db':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2018 Your name here, unless otherwise noted.
#
class db {
  include hosts
  class{'::mysql::server':
    package_name => 'mariadb-server',
    service_name => 'mariadb',
    root_password => 'encryptme',
    remove_default_accounts => true,
    restart => true,
    override_options => {
      'mysqld' => {
        'bind-address' => "${hostname}"
      }
    }
  }

  mysql::db{"todolist_${hostname}":
    user => 'todolist',
    password => 'todolist',
    host => "%",
    dbname => "todolist",
    require => Class['::mysql::server']
  }

}
