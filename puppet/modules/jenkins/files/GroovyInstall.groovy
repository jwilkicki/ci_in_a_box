import jenkins.*
import jenkins.model.*
import hudson.*
import hudson.model.*
import hudson.plugins.groovy.*
import hudson.tools.*

Groovy.DescriptorImpl groovyPlugin = Jenkins.get().getDescriptorByType(Groovy.DescriptorImpl.class)

InstallSourceProperty prop = new InstallSourceProperty([new GroovyInstaller("2.4.9")])

GroovyInstallation gInstall = new GroovyInstallation("Groovy 2.4.9","",[ prop ])

groovyPlugin.setInstallations(gInstall)
