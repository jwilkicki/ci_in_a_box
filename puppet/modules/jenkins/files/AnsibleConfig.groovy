import jenkins.*
import jenkins.model.*
import hudson.*
import hudson.model.*
import org.jenkinsci.plugins.ansible.*

AnsibleInstallation.DescriptorImpl ansiblePlugin = Jenkins.get().getDescriptorByType(AnsibleInstallation.DescriptorImpl.class)

AnsibleInstallation installer = new AnsibleInstallation("Ansible 2.4.2.0","/bin",null)

ansiblePlugin.setInstallations(installer)

ansiblePlugin.save()
