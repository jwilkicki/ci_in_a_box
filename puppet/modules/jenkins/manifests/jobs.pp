class jenkins::jobs {

  file{"/tmp/pipeline.xml":
    content => file('jenkins/pipeline_job.xml')
  }~>exec{"create pipeline":
    command => "/bin/cat /tmp/pipeline.xml | /usr/bin/java -jar /usr/local/bin/jenkins-cli.jar -s http://localhost:8080/ create-job todoList",
    unless => "/usr/bin/java -jar /usr/local/bin/jenkins-cli.jar -s http://localhost:8080/ get-job todoList",
    refreshonly => true
  }

}
