class jenkins::tools::ansible {

  package{'ansible':
    ensure => '2.4.2.0-2.el7'
  }

  file{'/tmp/AnsibleConfig.groovy':
    source => "puppet:///modules/jenkins/AnsibleConfig.groovy"
  }->jenkins::cli::command{"Configure Ansible":
    command => "-remoting groovy /tmp/AnsibleConfig.groovy",
    unless => "/usr/bin/xmllint --xpath '//name[contains(text(),\"Ansible 2.4.2.0\")]' /var/lib/jenkins/org.jenkinsci.plugins.ansible.AnsibleInstallation.xml"
  }


}
