class jenkins::tools::groovy {

  file{'/tmp/GroovyInstall.groovy':
    ensure => 'file',
    source => 'puppet:///modules/jenkins/GroovyInstall.groovy'
  }->jenkins::cli::command{"setup groovy":
    command => "-remoting groovy /tmp/GroovyInstall.groovy",
    unless => "/usr/bin/xmllint --xpath '//id[contains(text(),\"2.4.9\")]' /var/lib/jenkins/hudson.plugins.groovy.Groovy.xml"
  }

}
