class jenkins::credentials {

  file{"/tmp/domain.xml":
    content => file("jenkins/domain.xml"),
  }->file{"/tmp/credentials.xml":
    content => file("jenkins/credentials.xml"),
  }~>exec{"Restart service":
    command => "/usr/bin/systemctl restart jenkins",
    refreshonly=>true
  }~>jenkins::wait{"wait for credentials restart":
    refreshonly => true
  }->exec{"load domain":
    command => "/bin/cat /tmp/domain.xml | /usr/bin/java -jar /usr/local/bin/jenkins-cli.jar -s http://localhost:8080/ create-credentials-domain-by-xml system::system::jenkins",
    unless => "/usr/bin/java -jar /usr/local/bin/jenkins-cli.jar -s http://localhost:8080/ get-credentials-domain-as-xml system::system::jenkins git"
  }->exec{"load credentials":
    command => "/bin/cat /tmp/credentials.xml | /usr/bin/java -jar /usr/local/bin/jenkins-cli.jar -s http://localhost:8080/ create-credentials-by-xml system::system::jenkins git",
    unless => "/usr/bin/java -jar /usr/local/bin/jenkins-cli.jar -s http://localhost:8080/ get-credentials-as-xml system::system::jenkins git git-deploykey"
  }

  file{"/tmp/ansible-credentials.xml":
    content => file("jenkins/ansible-credentials.xml")
  }->exec{"load ansible credentials":
    command => "/bin/cat /tmp/ansible-credentials.xml | /usr/bin/java -jar /usr/local/bin/jenkins-cli.jar -s http://localhost:8080/ create-credentials-by-xml system::system::jenkins '(global)'",
    unless => "/usr/bin/java -jar /usr/local/bin/jenkins-cli.jar -s http://localhost:8080/ get-credentials-as-xml system::system::jenkins git ansible",
    refreshonly => true
  }

}
