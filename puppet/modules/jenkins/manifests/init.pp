class jenkins {

  include hosts

   # Install Jenkins repo file
   file{"/etc/yum.repos.d/jenkins.repo":
     ensure => 'file',
     source => "puppet:///modules/jenkins/jenkins.repo"
   }

   file{"/etc/pki/rpm-gpg/jenkins.io.key":
     ensure => 'file',
     source => "puppet:///modules/jenkins/jenkins.io.key",
     notify => Exec['jenkins rpm key import']
   }

   exec{'jenkins rpm key import':
     command => "/usr/bin/rpm --import /etc/pki/rpm-gpg/jenkins.io.key",
     refreshonly => true
   }

   package{'xorg-x11-server-Xvfb':
     ensure => 'installed'
   }

   package{"java":
     name => "java-1.8.0-openjdk",
     ensure => 'installed',
     require => Package['xorg-x11-server-Xvfb']
   }

   package{"git":
     ensure => 'installed'
   }

   # Install Jenkins
   package{"jenkins":
     ensure => 'installed',
     require => [File['/etc/yum.repos.d/jenkins.repo','/etc/pki/rpm-gpg/jenkins.io.key'], Package['java']]
   }

   augeas{"jenkins config":
      lens => 'Shellvars_list.lns',
      incl => '/etc/sysconfig/jenkins',
      require => Package['jenkins'],
      changes => [
        'set JENKINS_JAVA_OPTIONS/value[.=~regexp("-Djava.awt.headless=.*")] "-Djava.awt.headless=true"',
        'set JENKINS_JAVA_OPTIONS/value[.=~regexp("-Djenkins.install.runSetupWizard=.*")] "-Djenkins.install.runSetupWizard=false"'
      ],
      notify => Service['jenkins']
   }

   # Enable service and start
   service{"jenkins":
      ensure => 'running',
      enable => true,
   }~>jenkins::wait{"wait before loading plugins":
    refreshonly => true
   }

   # Install plugins:
   Jenkins::Plugin{
     require => [Service['jenkins'],Jenkins::Wait["wait before loading plugins"]]
   }

   jenkins::plugin{['git','job-dsl','workflow-aggregator','groovy','puppet','ansible','gradle']:

   }

   class{'jenkins::tools::groovy':
     require => [Service['jenkins'],Jenkins::Plugin['groovy']]
   }

   class{'jenkins::tools::ansible':
     require => [Service['jenkins'],Jenkins::Plugin['ansible']]
   }

   stage{"credentials":
      require => Stage["main"]
   }

   class{'jenkins::credentials':
      stage => "credentials"
   }

   stage{"jobs":
     require => Stage["credentials"]
   }

   class{'jenkins::jobs':
     stage => "jobs"
   }

}
