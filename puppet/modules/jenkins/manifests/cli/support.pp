class jenkins::cli::support {

  exec{"jenkins-cli":
    command => "/usr/bin/curl -o /usr/local/bin/jenkins-cli.jar http://localhost:8080/jnlpJars/jenkins-cli.jar",
    creates => '/usr/local/bin/jenkins-cli.jar',
    require => Service['jenkins']
  }


}
