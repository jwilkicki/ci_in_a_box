define jenkins::cli::command($command,$creates=undef,$unless=undef){

  include jenkins::cli::support

  exec{"${title}":
    command => "/usr/bin/java -jar /usr/local/bin/jenkins-cli.jar -s http://localhost:8080/ ${command}",
    creates => $creates,
    unless => $unless,
    require => Exec["jenkins-cli"]
  }

}
