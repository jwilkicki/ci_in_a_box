define jenkins::wait($refreshonly=false) {

  if ! defined(Package['wget']){
    package{'wget':
      ensure => 'installed'
    }
  }

  exec {"${title}":
    require => [Package['wget'],Service['jenkins']],
    command => "/usr/bin/wget  --spider http://${::hostname}:8080/cli/",
    tries => 30,
    try_sleep => 30,
    refreshonly => $refreshonly
  }

}
