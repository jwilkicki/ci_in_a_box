# Class: web
# ===========================
#
# Full description of class web here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'web':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2018 Your name here, unless otherwise noted.
#
class web {
  include hosts

  package {'httpd':
    ensure => 'installed'
  }

  $app_url = "http://app:8080/todoList"
  $docroot = "/var/www/app"

  file{$docroot:
    ensure => "directory"
  }->augeas{"/etc/httpd/conf/httpd.conf":
    lens => 'Httpd.lns',
    incl => "/etc/httpd/conf/httpd.conf",
    changes => [
      "set directive[.='ServerName'] ServerName",
      "set directive[.='ServerName']/arg ${::hostname}"
    ],
    notify => Service['httpd']
  }->file{"/etc/httpd/conf.d/app.conf":
    ensure => 'file',
    content => template("web/app.conf.erb"),
    require => Package['httpd'],
    notify => Service['httpd']
  }

  exec{"selinux enable http connect":
    command => "/sbin/setsebool -P httpd_can_network_connect=1",
    unless => "/sbin/getsebool httpd_can_network_connect | grep -q 'on$'"
  }

  service{'httpd':
    ensure => 'running',
    enable => true
  }

}
