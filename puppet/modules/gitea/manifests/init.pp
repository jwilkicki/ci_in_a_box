class gitea {

  include hosts

  exec{"ius repo rpm":
    command => "/bin/curl -o /tmp/ius-release.rpm https://dl.iuscommunity.org/pub/ius/stable/CentOS/7/x86_64/ius-release-1.0-15.ius.centos7.noarch.rpm",
    creates => "/tmp/ius-release.rpm"
  }->exec{"install ius repo rpm":
    command =>"/bin/yum localinstall -y /tmp/ius-release.rpm",
    unless => "/bin/rpm -q ius-release"
  }->package { 'git2u':
    ensure => installed,
  }

  user{"git":
    ensure => 'present',
    managehome => true
  }

  File{
    owner => 'git',
    group => 'git'
  }

  file{['/home/git','/home/git/gitea','/home/git/gitea/custom','/home/git/gitea/custom/conf']:
    ensure => 'directory',
    require => User["git"]
  }

  exec{'Download Gitea':
    command => '/usr/bin/curl -o /home/git/gitea/gitea https://dl.gitea.io/gitea/1.4/gitea-1.4-linux-amd64',
    creates => '/home/git/gitea/gitea',
    user => 'git',
    require => [User['git'],File['/home/git/gitea'],Package['git2u']]
  }

  file{'/home/git/gitea/gitea':
    ensure => 'file',
    mode => '0755',
    require => [File['/home/git/gitea'],Exec['Download Gitea']]
  }

  file{'/home/git/gitea/custom/conf/app.ini':
    ensure => 'file',
    mode => '0644',
    source => 'puppet:///modules/gitea/app.ini',
    require => File['/home/git/gitea/custom/conf'],
    replace => false,
    notify => Service['gitea']
  }

  file{'/etc/systemd/system/gitea.service':
    ensure => 'file',
    owner => 'root',
    group => 'root',
    mode => '0644',
    source => 'puppet:///modules/gitea/gitea.service',
    require => File['/home/git/gitea/gitea'],
    notify => Service['gitea']
  }

  service{'gitea':
    ensure => 'running',
    enable => 'true',
    require => File['/etc/systemd/system/gitea.service']
  }

}
