# Class: app
# ===========================
#
# Full description of class app here.
#
# Parameters
# ----------
#
# Document parameters here.
#
# * `sample parameter`
# Explanation of what this parameter affects and what it defaults to.
# e.g. "Specify one or more upstream ntp servers as an array."
#
# Variables
# ----------
#
# Here you should define a list of variables that this module would require.
#
# * `sample variable`
#  Explanation of how this variable affects the function of this class and if
#  it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#  External Node Classifier as a comma separated list of hostnames." (Note,
#  global variables should be avoided in favor of class parameters as
#  of Puppet 2.6.)
#
# Examples
# --------
#
# @example
#    class { 'app':
#      servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#    }
#
# Authors
# -------
#
# Author Name <author@domain.com>
#
# Copyright
# ---------
#
# Copyright 2018 Your name here, unless otherwise noted.
#
class app {

  include hosts

  user{'ansible':
    managehome => true,
    home => '/home/ansible',
    purge_ssh_keys => true
  }->file{['/home/ansible','/home/ansible/.ssh']:
    ensure => 'directory',
    owner => 'ansible',
    group => 'ansible',
    mode => '0700'
  }->ssh_authorized_key { 'jenkins@jenkins':
   user => 'ansible',
   type => 'ssh-rsa',
   key => strip(file('app/jenkins.pub'))
 }

 augeas{"enable ansible sudo":
   incl => '/etc/sudoers',
   lens => 'Sudoers.lns',
   changes => [
     "set spec[user='ansible']/user ansible",
     "set spec[user='ansible']/host_group/host ALL",
     "set spec[user='ansible']/host_group/command ALL",
     "set spec[user='ansible']/host_group/command/runas_user ALL",
     "set spec[user='ansible']/host_group/command/tag NOPASSWD"
   ]
 }

 package{'tomcat':
  ensure => 'installed',
  notify => Service['tomcat']
 }


 service{'tomcat':
   ensure => 'running',
   enable => true
 }

}
