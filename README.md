This project provides a complete CI/CD pipeline including a Gitea git server,
Jenkins build server, an Apache web server, a Tomcat application server, and a
MariaDB server. These servers are defined by a Vagrantfile that will provision
the servers with Puppet.

It also provides a Jenkins job that is pre-configured to pull an application from Gitea
using the jenkins and jenkins.pub keys sitting in the root of this project.

# License

The Vagrantfile, and the app, db, gitea, jenkins, hosts, and web Puppet modules in the puppet/modules
directory are all licensed under the BSD License.  The mysql, staging, and stdlib modules
are copied from the Puppet Forge in their entirety and are licensed under the terms included in their sub-directories by
their respective authors and are included for convenience with no ownership implied.

# Instructions

1. Clone this repository into a directory on your system.
2. Install [VirtualBox](https://virtualbox.org)
3. Install [Vagrant](https://vagrantup.com)
4. Run `vagrant up` to startup and provision virtual machines.  You will need Internet
   access to pull down the CentOS appliance from Vagrant's service and other items from
   the Internet.
5. Once the servers have started, you will need to configure the Gitea server.
   1. The git server is at http://192.168.50.2:3000/  
   2. Click the `Need an account? Sign up now` link to create an account. The account
      that is created will have administrator access.
   3. You may now create a repository and add the `jenkins.pub` key as deployment key.

# The Servers

This project creates five servers that represent a CI/CD build chain and a typical
three-tier web application, which is [available here](https://github.com/jwilkicki/todoList.git).  All servers are on the 192.168.50.0/24 network:

* Gitea http://192.168.50.2:3000
* Jenkins http://192.168.50.3:8080
* Apache http://192.168.50.4/
* Tomcat http://192.168.50.5:8080/
* MariaDB 192.168.50.6

The servers are setup to pull a repository from the sample organization todoList and deploy_playbook repositories.
